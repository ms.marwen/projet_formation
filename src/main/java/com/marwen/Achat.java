package com.marwen;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Achat {
	
	private Fournisseur fournisseur;
	private Article article;
	private float prixAchat;
	private int delai;
	
	
	
	@Override
	public String toString() {
		return "Achat [fournisseur=" + fournisseur + ", article=" + article + ", prixAchat=" + prixAchat + ", delai="
				+ delai + "]";
	}



	public Achat(Fournisseur fournisseur, Article article, float prixAchat, int delai) {
		super();
		this.fournisseur = fournisseur;
		this.article = article;
		this.prixAchat = prixAchat;
		this.delai = delai;
	}
	
	

}
