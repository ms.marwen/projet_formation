package com.marwen;

@FunctionalInterface
public interface ICreate<T> {
	
	public T createVoiture();

}
