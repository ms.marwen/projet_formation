package com.marwen;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter @Getter @NoArgsConstructor
public class Article {
	
	private int idArticle ;
	private String libelle;
	private int stock;
	private float prix;
	
	
	
	@Override
	public String toString() {
		return "Article [idArticle=" + idArticle + ", libelle=" + libelle + ", stock=" + stock + ", prix=" + prix + "]";
	}



	public Article(int idArticle, String libelle, int stock, float prix) {
		super();
		this.idArticle = idArticle;
		this.libelle = libelle;
		this.stock = stock;
		this.prix = prix;
	}

	
	
	
}
