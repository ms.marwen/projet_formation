package com.marwen;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Vector;

import org.springframework.scheduling.annotation.Async;

public class StreamPrinter {

	
	void print(Reader reader) throws IOException
	{
		
		int code = reader.read();
		while(code != -1)
		{	
			System.out.print( (char) code);
			code = reader.read();
		}
		reader.close();
	}
}
