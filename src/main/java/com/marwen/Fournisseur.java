package com.marwen;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Fournisseur {

	private int idFournisseur;
	private String nom;
	private String adresse;
	private String ville;
	
	
	@Override
	public String toString() {
		return "Fournisseur [idFournisseur=" + idFournisseur + ", nom=" + nom + ", adresse=" + adresse + ", ville="
				+ ville + "]";
	}


	public Fournisseur(int idFournisseur, String nom, String adresse, String ville) {
		super();
		this.idFournisseur = idFournisseur;
		this.nom = nom;
		this.adresse = adresse;
		this.ville = ville;
	}
	
	
}
