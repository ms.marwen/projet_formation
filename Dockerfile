FROM java:8-jdk-alpine
COPY ./target/Projet_formation-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "Projet_formation-0.0.1-SNAPSHOT.jar"]